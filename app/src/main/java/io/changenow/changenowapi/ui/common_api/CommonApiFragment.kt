package io.changenow.changenowapi.ui.common_api

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import io.changenow.api.model.CurrencyItem
import io.changenow.changenowapi.R
import kotlinx.android.synthetic.main.fragment_common.*

class CommonApiFragment : Fragment() {

    private val commonApiViewModel by viewModels<CommonApiViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_common, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btn_currency_list.setOnClickListener {
            commonApiViewModel.currencies<List<CurrencyItem>>(switch_fixed.isChecked) }
        btn_currency_to.setOnClickListener { commonApiViewModel.currencyTo<List<CurrencyItem>>(switch_fixed.isChecked) }
        btn_currency_info.setOnClickListener { commonApiViewModel.currencyInfo<List<CurrencyItem>>() }
        btn_tx_list.setOnClickListener { commonApiViewModel.listOfTransactions<List<CurrencyItem>>() }
        btn_tx_status.setOnClickListener { commonApiViewModel.txStatus<List<CurrencyItem>>() }
        btn_estimated.setOnClickListener { commonApiViewModel.estimated<List<CurrencyItem>>(switch_fixed.isChecked) }
        btn_create_tx.setOnClickListener { commonApiViewModel.createTx<List<CurrencyItem>>(switch_fixed.isChecked) }
        btn_pairs.setOnClickListener { commonApiViewModel.pairs<List<CurrencyItem>>(switch_fixed.isChecked) }
        btn_min_amount.setOnClickListener { commonApiViewModel.minAmount<List<CurrencyItem>>() }

        commonApiViewModel.responseData.observe(
            viewLifecycleOwner,
            Observer { text: String? ->
                text_response.text = text
            }
        )
    }
}