package io.changenow.changenowapi.ui.common_api

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import io.changenow.api.ChangenowApi
import io.changenow.api.model.MinAmount
import io.changenow.api.model.result.Result
import io.changenow.changenowapi.data.repository.ApiRepository
import io.changenow.changenowapi.ui.MainActivity.Companion.API_KEY
import kotlinx.coroutines.launch

class CommonApiViewModel : ViewModel() {

    val responseData: MutableLiveData<String> = MutableLiveData()
    private val changenowApi = ChangenowApi.Builder().apiKey(API_KEY).build()
    private val gson = Gson()
    private val apiRepository: ApiRepository

    init {
        apiRepository = ApiRepository(changenowApi)
    }

    fun <T> currencies(isFixedRate: Boolean) {
        responseData.value = ""
        viewModelScope.launch {
            when(val apiResult: Result<*>? = apiRepository.currencies<T>(isFixedRate)) {
                is Result.Success<*> ->
                    responseData.value = gson.toJson((apiResult as Result.Success<T>).data)
                is Result.Error -> responseData.value = apiResult.exception.message
            }
        }
    }

    fun <T> currencyTo(isFixedRate: Boolean) {
        responseData.value = ""
        viewModelScope.launch {
            when(val apiResult: Result<*>? = apiRepository.currenciesTo<T>(isFixedRate)) {
                is Result.Success<*> ->
                    responseData.value = "BTC:\n" + gson.toJson((apiResult as Result.Success<T>).data)
                is Result.Error -> responseData.value = apiResult.exception.message
            }
        }
    }

    fun <T> currencyInfo() {
        responseData.value = ""
        viewModelScope.launch {
            when(val apiResult: Result<*>? = apiRepository.currencyInfo<T>()) {
                is Result.Success<*> ->
                    responseData.value = "BTC:\n" + gson.toJson((apiResult as Result.Success<T>).data)
                is Result.Error -> responseData.value = apiResult.exception.message
            }
        }
    }

    fun <T> listOfTransactions() {
        responseData.value = ""
        viewModelScope.launch {
            when(val apiResult: Result<*>? = apiRepository.listOfTransactions<T>()) {
                is Result.Success<*> ->
                    responseData.value = gson.toJson((apiResult as Result.Success<T>).data)
                is Result.Error -> responseData.value = apiResult.exception.message
            }
        }
    }

    fun <T> txStatus() {
        responseData.value = ""
        viewModelScope.launch {
            when(val apiResult: Result<*>? = apiRepository.txStatus<T>()) {
                is Result.Success<*> ->
                    responseData.value = gson.toJson((apiResult as Result.Success<T>).data)
                is Result.Error -> responseData.value = apiResult.exception.message
            }
        }
    }

    fun <T> estimated(isFixedRate: Boolean) {
        responseData.value = ""
        viewModelScope.launch {
            when(val apiResult: Result<*>? = apiRepository.estimated<T>(isFixedRate)) {
                is Result.Success<*> ->
                    responseData.value = "BTC - ETH:\n" + gson.toJson((apiResult as Result.Success<T>).data)
                is Result.Error -> responseData.value = apiResult.exception.message
            }
        }
    }

    fun <T> createTx(isFixedRate: Boolean) {
        responseData.value = ""
        viewModelScope.launch {
            when(val apiResult: Result<*>? = apiRepository.createTx<T>(isFixedRate)) {
                is Result.Success<*> ->
                    responseData.value = gson.toJson((apiResult as Result.Success<T>).data)
                is Result.Error -> responseData.value = apiResult.exception.message
            }
        }
    }

    fun <T> pairs(isFixedRate: Boolean) {
        responseData.value = ""
        viewModelScope.launch {
            when(val apiResult: Result<*>? = apiRepository.pairs<T>(isFixedRate)) {
                is Result.Success<*> ->
                    responseData.value = gson.toJson((apiResult as Result.Success<T>).data)
                is Result.Error -> responseData.value = apiResult.exception.message
            }
        }
    }

    fun <T> minAmount() {
        responseData.value = ""
        viewModelScope.launch {
            when(val apiResult: Result<*>? = apiRepository.minAmount<T>()) {
                is Result.Success<*> ->
                    responseData.value = "BTC - ETH:\n" + "%.9f".format(((apiResult as Result.Success<T>).data as MinAmount?)?.minAmount ?: -1.0f)
                is Result.Error -> responseData.value = apiResult.exception.message
            }
        }
    }

}