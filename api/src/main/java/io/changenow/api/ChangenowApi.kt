package io.changenow.api

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import io.changenow.api.model.*
import io.changenow.api.model.result.CallParameters
import io.changenow.api.model.result.Result
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.io.IOException
import java.util.concurrent.TimeUnit

class ChangenowApi(apiKey: String, var changeNowManager: ChangeNowManager) {

    var apiKey: String = apiKey
        get() {
            require(field.isNotEmpty())
            return field
        }

    class Builder {

        private var apiKey = ""
        private lateinit var changeNowManager: ChangeNowManager

        fun apiKey(apiKey: String): Builder {
            require(apiKey.isNotEmpty()) { "apiKey must not be empty or null apiKey=$apiKey" }
            this.apiKey = apiKey
            return this
        }

        fun build(): ChangenowApi {
            val gson: Gson = GsonBuilder()
                .setLenient()
                .create()

            val builder: Retrofit.Builder = Retrofit.Builder()
                .baseUrl(API_URL)
                .client(newHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(CoroutineCallAdapterFactory())

            val retrofit: Retrofit = builder.build()

            changeNowManager = retrofit.create(ChangeNowManager::class.java)

            return ChangenowApi(apiKey, changeNowManager)
        }

        companion object {
            private val newHttpClient: OkHttpClient
                get() {
                    val client: OkHttpClient.Builder = OkHttpClient.Builder()
                        .followRedirects(true)
                        .followSslRedirects(true)
                        .retryOnConnectionFailure(true)
                        .cache(null)
                        .connectTimeout(10, TimeUnit.SECONDS)
                        .writeTimeout(10, TimeUnit.SECONDS)
                        .readTimeout(10, TimeUnit.SECONDS)
                    return client.build()
                }
        }
    }

    /** Single method  */
    suspend fun <T> apiWrapper(callName: CallName?, callParameters: CallParameters?): Result<*>? {
        callName ?: return Result.Error(IllegalArgumentException("methodName must not be null"))
        callParameters ?: return Result.Error(IllegalArgumentException("methodParameters must not be null"))

        return pickMethod<T>(callName, callParameters)
    }

    private suspend fun <T> pickMethod(
        callName: CallName,
        callParameters: CallParameters
    ): Result<*> {
        val deferred: Deferred<Response<T>>? = when (callName) {
            CallName.CURRENCIES -> getCurrencies(callParameters)
            CallName.CURRENCIES_TO -> getCurrenciesTo(callParameters)
            CallName.CURRENCY_INFO -> getCurrencyInfo(callParameters)
            CallName.LIST_OF_TRANSACTIONS -> getListOfTransactions(callParameters)
            CallName.TX_STATUS -> getTxStatus(callParameters)
            CallName.ESTIMATED -> getEstimated(callParameters)
            CallName.CREATE_TX -> getCreateTx(callParameters)
            CallName.PAIRS -> getPairs(callParameters)
            CallName.MIN_AMOUNT -> getMinAmount(callParameters)
        }

        deferred ?: return Result.Error(IOException("Error Occurred during getting safe Api result, Custom ERROR -"))

        return safeApiResult(
            call = { deferred.await() },
            errorMessage = "Error safeApiResult"
        )
    }

    private suspend fun <T> safeApiResult(call: suspend () -> Response<T>, errorMessage: String) : Result<*> {
        val response = call.invoke()
        if(response.isSuccessful) return Result.Success(
            response.body()!!
        )

        return Result.Error(IOException("Error Occurred during getting safe Api result, Custom ERROR - $errorMessage"))
    }

    private fun <T> getCurrencies(callParameters: CallParameters): Deferred<T> {
        return changeNowManager.currencies(
            callParameters.isActive,
            callParameters.isFixedRate
        ) as Deferred<T>
    }

    private fun <T> getCurrenciesTo(callParameters: CallParameters): Deferred<T> {
        requireNotNull(callParameters.tickerInfo) { "tickerInfo required" }
        return changeNowManager.currenciesTo(
            callParameters.tickerInfo,
            callParameters.isFixedRate
        ) as Deferred<T>
    }

    private fun <T> getCurrencyInfo(callParameters: CallParameters): Deferred<T> {
        requireNotNull(callParameters.tickerInfo) { "tickerInfo required" }
        return changeNowManager.currencyInfo(
            callParameters.tickerInfo
        ) as Deferred<T>
    }

    private fun <T> getListOfTransactions(callParameters: CallParameters): Deferred<T> {
        return changeNowManager.listOfTransactions(
            apiKey,
            callParameters.tickerFrom,
            callParameters.tickerTo,
            callParameters.txStatus,
            callParameters.limit,
            callParameters.offset,
            callParameters.dateFrom,
            callParameters.dateTo
        ) as Deferred<T>
    }

    private fun <T> getTxStatus(callParameters: CallParameters): Deferred<T> {
        requireNotNull(callParameters.txId) { "txId required" }
        return changeNowManager.txStatus(
            callParameters.txId,
            apiKey
        ) as Deferred<T>
    }

    private fun <T> getEstimated(callParameters: CallParameters): Deferred<T> {
        requireNotNull(callParameters.amount) { "amount required" }
        requireNotNull(callParameters.tickerFrom) { "tickerFrom required" }
        requireNotNull(callParameters.tickerTo) { "tickerTo required" }
        return if (callParameters.isFixedRate) {
            changeNowManager.estimatedFixes(
                callParameters.amount,
                callParameters.tickerFrom,
                callParameters.tickerTo,
                apiKey
            ) as Deferred<T>
        } else {
            changeNowManager.estimated(
                callParameters.amount,
                callParameters.tickerFrom,
                callParameters.tickerTo,
                apiKey
            ) as Deferred<T>
        }
    }

    private fun <T> getCreateTx(callParameters: CallParameters): Deferred<T> {
        requireNotNull(callParameters.tickerFrom) { "tickerFrom required" }
        requireNotNull(callParameters.tickerTo) { "tickerTo required" }
        requireNotNull(callParameters.address) { "address required" }
        requireNotNull(callParameters.amount) { "amount required" }
        return if (callParameters.isFixedRate) {
            changeNowManager.createFixedTx(
                    apiKey,
                    callParameters.tickerFrom,
                    callParameters.tickerTo,
                    callParameters.address,
                    callParameters.amount,
                    callParameters.extraId,
                    callParameters.refundAddress,
                    callParameters.refundExtraId,
                    callParameters.userId,
                    callParameters.payload,
                    callParameters.contactEmail
            ) as Deferred<T>
        } else {
            changeNowManager.createTx(
                apiKey,
                callParameters.tickerFrom,
                callParameters.tickerTo,
                callParameters.address,
                callParameters.amount,
                callParameters.extraId,
                callParameters.refundAddress,
                callParameters.refundExtraId,
                callParameters.userId,
                callParameters.payload,
                callParameters.contactEmail
            ) as Deferred<T>
        }
    }

    private fun <T> getPairs(callParameters: CallParameters): Deferred<T> {
        return if (callParameters.isFixedRate) {
            changeNowManager.listOfFixed(apiKey) as Deferred<T>
        } else {
            changeNowManager.pairs(
                callParameters.isIncludePartners
            ) as Deferred<T>
        }
    }

    private fun <T> getMinAmount(callParameters: CallParameters): Deferred<T> {
        requireNotNull(callParameters.tickerFrom) { "tickerFrom required" }
        requireNotNull(callParameters.tickerTo) { "tickerTo required" }
        return changeNowManager.minAmount(
            callParameters.tickerFrom,
            callParameters.tickerTo
        ) as Deferred<T>
    }

    interface ChangeNowManager {
        /** Common methods  */
        @GET("currencies")
        fun currencies(
            @Query("active") isActive: Boolean = true,
            @Query("fixedRate") fixedRate: Boolean = false
        ): Deferred<Response<List<CurrencyItem>>>

        @GET("currencies-to/{ticker}")
        fun currenciesTo(
            @Path("ticker") ticker: String,
            @Query("fixedRate") fixedRate: Boolean = false
        ): Deferred<Response<List<CurrencyItem>>>

        @GET("currencies/{ticker}")
        fun currencyInfo(
            @Path("ticker") ticker: String
        ): Deferred<Response<CurrencyItem>>

        @GET("transactions/{apiKey}")
        fun listOfTransactions(
            @Path("apiKey") apiKey: String,
            @Query("from") tickerFrom: String?,
            @Query("to") tickerTo: String?,
            @Query("status") status: String?,
            @Query("limit") limit: Int = 50,
            @Query("offset") offset: Int = 0,
            @Query("dateFrom") dateFrom: String?,
            @Query("dateTo") dateTo: String?
        ): Deferred<Response<List<TxItem>>>

        @GET("transactions/{id}/{apikey}")
        fun txStatus(
            @Path("id") id: String,
            @Path("apikey") apikey: String
        ): Deferred<Response<TxItem>>

        /** Standard Flow (Floating Rate)  */
        @GET("exchange-amount/{amount}/{from}_{to}")
        fun estimated(
            @Path("amount") amount: String,
            @Path("from") from: String,
            @Path("to") to: String,
            @Query("api_key") apikKey: String
        ): Deferred<Response<Estimated>>

        @POST("transactions/{apiKey}")
        @FormUrlEncoded
        fun createTx(
            @Path("apiKey") apiKey: String,
            @Field("from") from: String,
            @Field("to") to: String,
            @Field("address") address: String,
            @Field("amount") amount: String,
            @Field("extraId") extraId: String?,
            @Field("refundAddress") refundAddress: String?,
            @Field("refundExtraId") refundExtraId: String?,
            @Field("userId") userId: String?,
            @Field("payload") payload: String?,
            @Field("contactEmail") contactEmail: String?
        ): Deferred<Response<TxItem>>

        @GET("market-info/available-pairs")
        fun pairs(
            @Query("includePartners") includePartners: Boolean = true
        ): Deferred<Response<List<String>>>

        @GET("min-amount/{from}_{to}")
        fun minAmount(
            @Path("from") from: String,
            @Path("to") to: String
        ): Deferred<Response<MinAmount>>

        /** Fixed-Rate Flow  */
        @GET("market-info/fixed-rate/{apiKey}")
        fun listOfFixed(
            @Path("apiKey") apiKey: String
        ): Deferred<Response<List<FixRateMarket>>>

        @GET("exchange-amount/fixed-rate/{sendAmount}/{from}_{to}")
        fun estimatedFixes(
            @Path("sendAmount") sendAmount: String,
            @Path("from") from: String,
            @Path("to") to: String,
            @Query("api_key") apiKey: String
        ): Deferred<Response<Estimated>>

        @POST("transactions/fixed-rate/{apiKey}")
        @FormUrlEncoded
        fun createFixedTx(
            @Path("apiKey") apiKey: String,
            @Field("from") from: String,
            @Field("to") to: String,
            @Field("address") address: String,
            @Field("amount") amount: String,
            @Field("extraId") extraId: String?,
            @Field("refundAddress") refundAddress: String?,
            @Field("refundExtraId") refundExtraId: String?,
            @Field("userId") userId: String?,
            @Field("payload") payload: String?,
            @Field("contactEmail") contactEmail: String?
        ): Deferred<Response<TxItem>>
    }

    companion object {
        private const val API_URL = "https://changenow.io/api/v1/"
    }

}